#!/bin/bash


## Teil 2
#wget https://gitlab.com/klausz/djoscar-shinst/-/raw/main/10-djawagoscngi.sh
#chmod +x 10_wagoscnginx.sh
#sudo ./10_wagoscnginx.sh


#!/bin/bash

echo "Beginne mit der Installation von Django, Wagtail und Django Oscar..."

## Stelle sicher, dass das Virtual Environment aktiv ist
#if [[ "$VIRTUAL_ENV" == "" ]]; then
#    echo "Das Virtual Environment ist nicht aktiviert. Bitte aktiviere es mit 'source /opt/pollwerkshop/venv/bin/activate' und führe dieses #Skript erneut aus."
#    exit 1
#fi

# Wheel installieren
pip install wheel

# Installiere Django, Wagtail und Django Oscar
pip install "Django~=4.2" wagtail 'django-oscar[sorl-thumbnail]'

# Wagtail-Projekt erstellen
cd /opt/pollwerkshop
wagtail start pollwerkshop

# Gehe ins Projektverzeichnis und führe Migrationen durch
cd pollwerkshop
./manage.py migrate
./manage.py collectstatic --noinput --clear


# Nginx konfigurieren - Hier die IP-Adresse eintragen
echo "Konfiguriere Nginx. Ersetze 'server_name deinserver.de;' mit deiner Server-IP oder Domain."
read -p "Drücke Enter, um die Nginx-Konfigurationsdatei zu bearbeiten..."

# Hier deine Server-IP oder Domain eintragen
SERVER_IP="65.109.168.3" # Ersetze dies durch deine tatsächliche IP-Adresse oder Domain

cat > /etc/nginx/sites-available/pollwerkshop <<EOF
server {
    listen 80;
    server_name $SERVER_IP;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        alias /opt/pollwerkshop/static/;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/opt/pollwerkshop/pollwerkshop.sock;
    }
}
EOF

# Aktiviere die Nginx-Konfiguration und starte Nginx neu
ln -s /etc/nginx/sites-available/pollwerkshop /etc/nginx/sites-enabled
nginx -t && systemctl restart nginx


echo "Die Installation ist abgeschlossen."
