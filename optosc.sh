#!/bin/bash

#adduser klaus
#adduser klaus sudo
#logout
#login klaus

#wget https://gitlab.com/klausz/djoscar-shinst/-/raw/main/optosc.sh
#chmod +x optosc.sh
#sudo ./optosc.sh


# Definiere den Pfad für das Projektverzeichnis und das Logfile
#PROJECT_DIR="/opt/pollwerkshop"
PROJECT_DIR="/opt/djaosc"
LOGFILE="$PROJECT_DIR/install.log"

echo $PROJECT_DIR 
echo $LOGFILE
read -p "?->"

# Sicherstellen, dass das Skript mit sudo-Rechten ausgeführt wird
if [[ "$(id -u)" != "0" ]]; then
    echo "Dieses Skript muss mit sudo-Rechten ausgeführt werden." >&2
    exit 1
fi

#came afterwards, called ensurepip
apt update
apt upgrade
apt install python3.10-venv

#20240325 Ich weis nicht ob das unterging oder vergessen war; aber sonst kann ich pollwerkshop nicht installiern
# apt install python3-django 

# Erstelle das Projektverzeichnis und das Logfile
sudo mkdir -p $PROJECT_DIR
sudo touch $LOGFILE

# Ändere den Besitzer des Projektverzeichnisses zum aktuellen Benutzer
sudo chown $USER:$USER $PROJECT_DIR
sudo chown $USER:$USER $LOGFILE

# Starte das Logging
exec > >(tee -a "$LOGFILE") 2>&1

echo "Beginn der Installation..."

# Wechsle in das Projektverzeichnis; das virteille environment heisst pollwerkshop
cd $PROJECT_DIR

# Erstelle das Virtual Environment
#python -m venv /path/to/new/virtual/environment
python3 -m venv envoscar
#python3 -m venv pollwerkshop

# Aktiviere das Virtual Environment
source envoscar/bin/activate
#source pollwerkshop/bin/activate

echo "VirtualEnv Name: "
echo $VIRTUAL_ENV 
read -p "?->"

# Überprüfe, ob das Virtual Environment aktiv ist
if [[ "$VIRTUAL_ENV" != "" ]]; then
    echo "Virtual Environment ist aktiviert."
else
    echo "Fehler: Virtual Environment konnte nicht aktiviert werden."
    exit 1
fi

# Upgrade pip und stelle sicher, dass wheel installiert ist
pip install --upgrade pip

# pip install Gunicorn

pip install wheel

# Installiere Django Oscar
pip install django-oscar

# wird im Settings.py bei Sandbox verwendet
pip install django-environ

pip install sorl-thumbnail
pip install django-extensions
pip install django-debug-toolbar
pip install Whoosh
pip install whitenoise

# apt install sqlite3 #nicht zwingend, aber um änderungen an der db anzubringen, initialer superuser

# Erstelle ein neues Oscar-Projekt namens 'pollwerkshop' #kein punkt bei denen
# django-admin startproject frobshop

django-admin startproject projpollwerk


chown -R klaus:klaus /opt/djaosc

exit




# Wechsle in das Projektverzeichnis; Fehler: 1. Kann nicht automatisch in das VZ wechseln. Um Migrate zu starte muss ich das Virtuelle nochmals # starten bin da nicht mehr drin 
# pollwerkshop #da ist ein Fehler geht immer noch nicht
echo "Wechsel ins Projecktverzeichnis" 
cd /opt
source envoscar/bin/activate
# cd $PROJECT_DIR  
# echo $PROJECT_DIR
read -p "?->"

# Führe Django-Migrationen aus
./manage.py migrate
read -p "?->"

# hier gans ne Fehlermeldung weil /opt/djaosc/pollwerkshop/public static nicht existierte -> aber 291 files copied, 813 post processed 
./manage.py collectstatic --noinput

python manage.py runserver '0.0.0.0:8000'


exit

# Sammle statische Dateien, hier passiert wenig; Fehler:Using staticfiles app withot having set STATIC_ROOT (kann sein ich muss die Daten
# tauschen); da habe ich aktuel einige Fehlermeldungen 
#funktioniert, zuvor musste ich aber chown -R klaus:klaus pollwerkshop durchführen (evtl unsicher)
./manage.py collectstatic --noinput
#klappt 265 static files copied to -opt-djaosc-pollwerkshop
read -p "?->"

#hier muss ich im Verzeicjhnis sein und ich uss das Virt Env aktv haben. Achunt wegen ; und :

gunicorn pollwerkshop.wsgi:application --bind 111.112.113.114:8000

echo "Die Installation von Django Oscar in 'pollwerkshop' ist abgeschlossen. NGinx wird nicht installiert und gestartet, Servei nicht gestartet"
# Aktivieren: source pollwerkshop/bin/activate danach kann ich 
# ./manage.py migrate
# ./manage.py runserver
#was fehler hier für dateien? 
# Hier mus sich Gunicorn installieren. Evtl hier oder auf dem zweiten File for nginx
# wo und was muss ich bei ALLOWED_HOSTS eingeben?

sudo apt-get install -y nginx



python manage.py runserver #funktioniert :-) #ich bin aber nicht in virtualenv
#nohup python.py runserver #sollte gehen, zunächst aber schauen ob es ohne nohup geht; mit ctrl+C zu unterbrechen

#webbrowser 
sudo apt install links 
#aufruf links pollwerk.com


#nginx config file; hier wird eine neue virtuelle webseite erstell; die nginx.con wird nicht geändert
# Konfiguriere Nginx als Reverse-Proxy
echo "Nginx wird konfiguriert..."
cat > /etc/nginx/sites-available/django_oscar <<EOF
server {    
	listen 80;    
	server-name -;    

	location = /favicon.ico { access-log off; log-not-found off; }    
	
	location /static/ {        
#		root /home/django/django-osca/sandbox;
		klaus /opt/pollwerkshop/lib/python3.10/site-packages/oscar/static/oscar;
#		klaus /opt/djaosc/pollwerkshop
		}	    
		
		location / {        
			include proxy-params;        
			proxy-pass http://unix;/home/django/django-oscar/sandbox/oscar.sock;    
		}
}

EOF



# ein config file wurde früher erstellt und muss vorhanden sein 
# Aktiviere die Nginx-Konfiguration und starte Nginx neu
#erstellen des symbolischen links
sudo ln -s /etc/nginx/sites-available/django_oscar /etc/nginx/sites-enabled

#start
sudo nginx -t && systemctl restart nginx


#server mit 0.0.0.0:80xx starten - dann kann ich von aussen zugreifen
#ich sehe die django seite nur weil ich im confid DBUG = True gesetzt habe. Muss später raus. Wie aber kommme ich an die Oscar Seite? 
#STATIS_ROOT habe ich hartcodiert auf STSTIC_ROOR = 'OPT/DJOSC/POLL..'

# was erstaunlich ist: mit http://65.21.55.105:8000/catalogue/ kann ich den Webshop - ohne dass de Django Server läuft die Seite aufrufen. 
# die config/gunicorn/dev.py ist sehr wichtig. Einstellung: pollwerkshop.wsgi:applikation 

# ergänzungen:
# die lese/schreiberechtigungen sind auf allen pfaden ok
# ich musste - nach dem Wechsel ins env das propollwerk anlegen
# dann konnte ich apps auf 2 verschiedenen ebenen anlegen. Möglicherweise ging da eine Datei verloren