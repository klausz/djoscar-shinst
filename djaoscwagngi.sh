#!/bin/bash

#adduser klaus
#adduser klaus sudo
#logout
#login klaus

#download script from github etc # da muss ich noch schauen

#wget https://gitlab.com/klausz/djoscar-shinst/-/raw/main/djaoscwagngi.sh

# Sicherstellen, dass das Skript mit sudo ausgeführt wird
if [[ "$(id -u)" != "0" ]]; then
    echo "Dieses Skript muss mit sudo-Rechten ausgeführt werden." >&2
    exit 1
fi

LOGFILE="/var/log/django_oscar_wagtail_install.log"
exec > >(tee -a "$LOGFILE") 2>&1

echo "Beginn der Installation..."

# Systemaktualisierungen
echo "Aktualisiere das System..."
apt-get update && apt-get upgrade -y

# Notwendige Pakete installieren
echo "Installiere notwendige Pakete..."
apt-get install -y python3-pip python3-dev python3-venv libpq-dev nginx git build-essential libssl-dev libffi-dev

# Installiere wheel global
pip3 install wheel

# Projektverzeichnis erstellen und Virtual Environment einrichten
echo "Erstelle Projektverzeichnis und richte Virtual Environment ein..."
mkdir -p /opt/pollwerkshop
cd /opt/pollwerkshop

## AB HIER MANUELL

#python3 -m venv venv
sudo python3 -m venv venv

echo "Das Virtual Environment wurde erstellt. Bitte aktiviere es manuell mit dem Befehl 'source venv/bin/activate', und führe dann 'pip install wheel' aus. Drücke Enter, wenn du fertig bist."
read -p ""

source venv/bin/activate #ging ohne Sudo, ist der notwendig? 

sudo pip install wheel


# Hinweis zur manuellen Fortsetzung
echo "Fahre mit der Installation fort, nachdem das Virtual Environment aktiviert und Wheel installiert wurde. Drücke Enter, um fortzufahren."
read -p ""

# Django explizit auf eine mit Oscar kompatible Version setzen
#pip install "Django~=4.2"

# Wagtail installieren
echo "Installiere Wagtail..."
#pip install wagtail
sudo pip install wagtail

# sorl-thumbnail installieren
echo "Installiere sorl-thumbnail..."
#pip install sorl-thumbnail
sudo pip install sorl-thumbnail

# Django Oscar mit sorl-thumbnail installieren
echo "Installiere Django-Oscar mit sorl-thumbnail..."
#pip install 'django-oscar[sorl-thumbnail]'
sudo pip install 'django-oscar[sorl-thumbnail]'

# Erstelle ein neues Wagtail-Projekt mit dem Namen pollwerkshop
echo "Erstelle das Wagtail-Projekt 'pollwerkshop'..."
# wagtail start pollwerkshop .
wagtail start pollwerkshop .

# Gehe ins Projektverzeichnis
#cd pollwerkshop #bin ich schon drin 

# Migrationen durchführen und statische Dateien sammeln; funktioniert. Ich muss im Verzeichnis sein manage.py ist
echo "Führe Migrationen durch und sammle statische Dateien..."
./manage.py migrate
./manage.py collectstatic --noinput --clear #dann wird es nach home-klaus-static kopiert

# Nginx konfigurieren - Hier die IP-Adresse eintragen
echo "Konfiguriere Nginx. Ersetze 'server_name deinserver.de;' mit deiner Server-IP oder Domain."
read -p "Drücke Enter, um die Nginx-Konfigurationsdatei zu bearbeiten..."

# Hier deine Server-IP oder Domain eintragen
SERVER_IP="deinserver.de" # Ersetze dies durch deine tatsächliche IP-Adresse oder Domain

cat > /etc/nginx/sites-available/pollwerkshop <<EOF
server {
    listen 80;
    server_name $SERVER_IP;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        alias /opt/pollwerkshop/static/;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/opt/pollwerkshop/pollwerkshop.sock;
    }
}
EOF

# Aktiviere die Nginx-Konfiguration und starte Nginx neu
ln -s /etc/nginx/sites-available/pollwerkshop /etc/nginx/sites-enabled
nginx -t && systemctl restart nginx

echo "Installation abgeschlossen. Bitte überprüfe das Logfile für Details: $LOGFILE"
