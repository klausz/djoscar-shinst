#!/bin/bash

# Skript zur Installation von Nginx auf Ubuntu 22.04

# Stelle sicher, dass das Skript als Root (oder mit Root rechten) ausgeführt wird
if [ "$(id -u)" != "0" ]; then
   echo "Dieses Skript muss als Root oder mit root-rechten ausgeführt werden" 1>&2
   exit 1
fi

wget https://gitlab.com/klausz/djoscar-shinst/-/raw/main/aussenzugang.sh

#chmod +x aussenzugang.sh
#sudo ./aussenzugang.sh

apt-get install -y nginx git


# Konfiguriere Nginx als Reverse-Proxy
echo "Nginx wird konfiguriert..."
cat > /etc/nginx/sites-available/django_oscar <<EOF
server {
    listen 80;
    server_name _;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
#        root /home/django/django-oscar/sandbox;
    	klaus /home/pollwerkshop;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/home/django/django-oscar/sandbox/oscar.sock;
    }
}
EOF

# Aktiviere die Nginx-Konfiguration und starte Nginx neu
ln -s /etc/nginx/sites-available/django_oscar /etc/nginx/sites-enabled
nginx -t && systemctl restart nginx

echo "Nginx wurde erfolgreich installiert und konfiguriert."

#lokaler test
# sudo apt install w3m #aufruf mit w3m URL
# sudo apr install elinks
