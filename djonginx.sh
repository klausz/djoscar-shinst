#!/bin/bash

# Skript zur Installation von Django Oscar und Nginx auf Ubuntu 22.04

# Stelle sicher, dass das Skript als Root (oder mit Root rechten) ausgeführt wird
if [ "$(id -u)" != "0" ]; then
   echo "Dieses Skript muss als Root ausgeführt werden" 1>&2
   exit 1
fi

#rm -r #removes directory with content
#adduser klaus
#adduser klaus sudo
#logout
#login klaus

#download script from github etc # da muss ich noch schauen

#wget https://www.dropbox.com/scl/fo/nov01cmrtzdzl88ure8yn/djonginx.sh
wget https://gitlab.com/klausz/djoscar-shinst/-/raw/main/djonginx.sh

#chmod +x djonginx.sh
#sudo ./djonginx.sh #auch als user mit sudo rechten muss ich sudo vor den Befehlsaufruf setzen; evtl nicht bei root
#als root kann ich es ohne sudo aufrufen    


# Aktualisiere das System - besser als root nachdem klaus mit Sudo einiges nicht darf
echo "System wird aktualisiert..."
sudo apt-get update && apt-get upgrade -y

# Installiere notwendige Pakete
echo "Notwendige Pakete werden installiert..."

sudo apt-get install -y python3-pip python3-dev python3-venv libpq-dev nginx git



# Erstelle einen Benutzer für das Django-Projekt
#echo "Django-Benutzer wird erstellt..."
#useradd -m -s /bin/bash Django
#usermod -aG sudo django

# Wechsle zum Django-Benutzer
#su - django <<'EOF'

##Virtualenv fällt raus da der User nach dem Script nicht im Virtual Env ist; Server hat wenig Python Varianten
# nacholgendes als klaus installieren; hat gut geklappt. Bin in Virtualenv ()
# mkdir py3djaoscar - das zusätzliche Unterverzeichnis scheint nicht notwendig
#mit virtualenv gibt es probleme da ich zeitweise im Ladescript den Sudo brauche und zeitweise nicht
sudo python3 -m pip install virtualenv #sudo hier nicht aber weiter unten
# the script virtualenv is installed in '-home-klaus-.local-bin' which is not path - add to path?

export PATH=/home/klaus/.local/bin:$PATH

export PATH=/bin/usr/bin:$PATH

sudo python3 -m virtualenv envdjoscar #sudo notw
#nachfolgend das geht nicht wenn ich sudo vorne dran setze, das script läuft aber unter sudo; kann ich es ohne sudo starten da klaus dabei ist; oder kann ich #da sudo beenden
source envdjoscar/bin/activate

# mkdir envdjoscar

cd envdjoscar

#EOF

exit


sudo pip install wagtail #evtl außerhalb des virtualenv; problem, das installiert ein sehr aktuelles Django; 

sudo pip install sorl-thumbnail

sudo pip install 'django-oscar[sorl-thumbnail]'

sudo wagtail start pollwerkshop .  

#erstellt das Verzeichnis pollwerkshop, vermutlich sollte der Befehl aus dem Verzeichnis klaus aufgerufen werden

sudo mv pollwerkshop/settings/base.py pollwerkshop/settings/base_old.py
chmod 777 pollwerkshop/settings
#change dir

#wget https://www.dropbox.com/scl/fo/nov01cmrtzdzl88ure8yn/base.py
wget -O pollwerkshop/settings/base.py https://gitlab.com/klausz/djoscar-shinst/-/raw/main/base.py

chmod 755 pollwerkshop/settings #muss das recht am Verzeichis geändert werden?

mv pollwerkshop/urls.py pollwerkshop/urls_old.py
chmod 777 pollwerkshop/
wget -O pollwerkshop/urls.py https://gitlab.com/klausz/djoscar-shinst/-/raw/main/urls.py

chmod 755 pollwerkshop/

#hochladen 1 Datei 
# sudo chmod 755 pollwerkshop 


# EOF 
#besser abosulte adressierung dann läuft weniger schief
mv /home/klaus/envdjoscar/lib/python3.10/site-packages/oscar/config.py /home/klaus/envdjoscar/lib/python3.10/site-packages/oscar/config_old.py

wget -O /home/klaus/envdjoscar/lib/python3.10/site-packages/oscar/config.py https://gitlab.com/klausz/djoscar-shinst/-/raw/main/config.py


mv /home/klaus/envdjoscar/lib/python3.10/site-packages/oscar/defaults.py /home/klaus/envdjoscar/lib/python3.10/site-packages/oscar/defaults_old.py

wget -O /home/klaus/envdjoscar/lib/python3.10/site-packages/oscar/defaults.py https://gitlab.com/klausz/djoscar-shinst/-/raw/main/defaults.py

# exit




## problem: Config.py + defaults.py -- mir fehlt der Pfad.


#wget https://www.dropbox.com/scl/fo/nov01cmrtzdzl88ure8yn/defaults.py
#wget https://gitlab.com/klausz/djoscar-shinst/-/raw/main/defaults.py

#hochladen der 2 Dateien
# sudo chmod 755 envdjoscar/lib/python3.10/site-packages/oscar
# sudo chmod 777 pollwerkshop

pip install whitenoise 

#hochladen (ich muss zunächst die Verzeichhnisrechte von 755 auf 777 ändern; dann die alten Dateien umbenennen ):
#nach /home/klaus/pollwerkshop/settings.base.py
#nach /home/klaus/envdjoscar/lib/python3.10/site-packages/oscar #1. Config 2. Defaultsr 
#nach /home/klaus/pollwerkshop/urls.py

## hier muss ich doch 2 Verezichnisse löschen, ich bin in /home/klaus
rm -r search #/home/klaus/search
rm -r home #/home/klaus/home



python manage.py migrate 

python manage.py createsuperuser

# Klone das Oscar-Beispielprojekt
#echo "Oscar-Beispielprojekt wird geklont..."
#git clone https://github.com/django-oscar/django-oscar.git
#cd django-oscar

# Erstelle eine virtuelle Umgebung und aktiviere sie
#echo "Virtuelle Umgebung wird erstellt..."
#python3 -m venv oscar-env
##fehler, war nicht drin
#source oscar-env/bin/activate

# Installiere Django Oscar
#echo "Django Oscar wird installiert..."
#pip install -r requirements.txt

# Initialisiere die Datenbank
#echo "Datenbank wird initialisiert..."
#cd sandbox
#./manage.py migrate

# Sammle statische Dateien
#echo "Statische Dateien werden gesammelt..."
#./manage.py collectstatic --noinput

# Starte den Development-Server (nur zu Testzwecken, nicht für den Produktivbetrieb)
#echo "Development-Server wird gestartet..."
#./manage.py runserver 0.0.0.0:8000

#brauche ich nachfolgendes
#cp -a  /Library/Frameworks/Python.framework/Versions/3.12/lib/python3.12/site-packages/oscar/static/oscar/* pollwerkshop/static
# cp -a /envdjoscar/lib/python3.10/site-packages/oscar/static/oscar/* pollwerkshop/static
## brauch ich das?
# cp -a envdjoscar/lib/python3.10/site-packages/oscar/static/oscar/* pollwerkshop/static

python manage.py runserver #funktioniert :-) #ich bin aber nicht in virtualenv
#nohup python.py runserver #sollte gehen, zunächst aber schauen ob es ohne nohup geht; mit ctrl+C zu unterbrechen

#webbrowser 
sudo apt install links 
#aufruf links pollwerk.com

exit

#nginx config file; hier wird eine neue virtuelle webseite erstell; die nginx.con wird nicht geändert
# Konfiguriere Nginx als Reverse-Proxy
echo "Nginx wird konfiguriert..."
cat > /etc/nginx/sites-available/django_oscar <<EOF
server {
    listen 80;
    server_name _;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/django/django-oscar/sandbox;
#	klaus /home/
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/home/django/django-oscar/sandbox/oscar.sock;
    }
}
EOF



# ein config file wurde früher erstellt und muss vorhanden sein 
# Aktiviere die Nginx-Konfiguration und starte Nginx neu
#erstellen des symbolischen links
ln -s /etc/nginx/sites-available/django_oscar /etc/nginx/sites-enabled

#start
nginx -t && systemctl restart nginx

echo "Django Oscar und Nginx wurden erfolgreich installiert und konfiguriert."

#lokaler test
# sudo apt install w3m #aufruf mit w3m URL
# sudo apr install elinks
# wget https://www.dropbox.com/s/ew2jket9lisdf4oor/example.zip
