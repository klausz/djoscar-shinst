#!/bin/bash

#wget https://gitlab.com/klausz/djoscar-shinst/-/raw/main/05-venv.sh
#chmod +x 05_venv.sh
#sudo ./05_venv.sh
##danach Teil 2

# Sicherstellen, dass das Skript mit sudo ausgeführt wird
if [[ "$(id -u)" != "0" ]]; then
    echo "Dieses Skript muss mit sudo-Rechten ausgeführt werden." >&2
    exit 1
fi

LOGFILE="/var/log/django_oscar_wagtail_setup.log"
exec > >(tee -a "$LOGFILE") 2>&1

echo "Beginn der Setup-Routine..."

# Systemaktualisierungen
echo "Aktualisiere das System..."
apt-get update && apt-get upgrade -y

# Notwendige Pakete installieren
echo "Installiere notwendige Pakete..."
apt-get install -y python3-pip python3-dev python3-venv libpq-dev nginx git build-essential libssl-dev libffi-dev

# Projektverzeichnis erstellen
echo "Erstelle Projektverzeichnis..."
mkdir -p /opt/pollwerkshop
cd /opt/pollwerkshop

# Virtual Environment einrichten
echo "Richte Virtual Environment ein..."
python3 -m venv venv

echo "Das Setup ist abgeschlossen. Bitte aktiviere das Virtual Environment manuell: source /opt/pollwerkshop/venv/bin/activate und führe dann das Installationsskript aus."