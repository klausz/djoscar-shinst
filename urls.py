#pollwerkshop/urls.py
# wget https://gitlab.com/klausz/djoscar-shinst/-/raw/main/urls.py


from django.conf import settings
from django.urls import include, path
from django.contrib import admin

from django.apps import apps

# from search import views as search_views

urlpatterns = [
    path("django-admin/", admin.site.urls),
    path('',include(apps.get_app_config('oscar').urls[0]))
]


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = [
    path('admin/', admin.site.urls),
]
