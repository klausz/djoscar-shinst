#!/bin/bash

# Skript zur Installation von Nginx auf Ubuntu 22.04

# Stelle sicher, dass das Skript als Root (oder mit Root rechten) ausgeführt wird
if [ "$(id -u)" != "0" ]; then
   echo "Dieses Skript muss als Root ausgeführt werden" 1>&2
   exit 1
fi


#wget https://gitlab.com/klausz/djoscar-shinst/-/raw/main/ngi.sh

#chmod +x ngi.sh
#sudo ./ngi.sh 


# Aktualisiere das System - besser als root nachdem klaus mit Sudo einiges nicht darf
echo "System wird aktualisiert..."
sudo apt-get update && apt-get upgrade -y

# Installiere notwendige Pakete
echo "Notwendige Pakete werden installiert..."

sudo apt-get install -y nginx



python manage.py runserver #funktioniert :-) #ich bin aber nicht in virtualenv
#nohup python.py runserver #sollte gehen, zunächst aber schauen ob es ohne nohup geht; mit ctrl+C zu unterbrechen

#webbrowser 
sudo apt install links 
#aufruf links pollwerk.com


#nginx config file; hier wird eine neue virtuelle webseite erstell; die nginx.con wird nicht geändert
# Konfiguriere Nginx als Reverse-Proxy
echo "Nginx wird konfiguriert..."
cat > /etc/nginx/sites-available/django_oscar <<EOF
server {    
	listen 80;    
	server-name -;    

	location = /favicon.ico { access-log off; log-not-found off; }    
	
	location /static/ {        
#		root /home/django/django-osca/sandbox;
		klaus /opt/djaosc/pollwerkshop/lib/python3.10/site-packages/oscar/static/oscar;
#		klaus /opt/djaosc/pollwerkshop
		}	    
		
		location / {        
			include proxy-params;        
			proxy-pass http://unix;/home/django/django-oscar/sandbox/oscar.sock;    
		}
}

EOF



# ein config file wurde früher erstellt und muss vorhanden sein 
# Aktiviere die Nginx-Konfiguration und starte Nginx neu
#erstellen des symbolischen links
sudo ln -s /etc/nginx/sites-available/django_oscar /etc/nginx/sites-enabled

#start
sudo nginx -t && systemctl restart nginx

echo "Django Oscar und Nginx wurden erfolgreich installiert und konfiguriert."

