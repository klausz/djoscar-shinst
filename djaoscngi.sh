#!/bin/bash

#wget https://gitlab.com/klausz/djoscar-shinst/-/raw/main/djaoscngi.sh

#chmod +x djaoscngi.sh

# sudo ./djaoscngi.sh

#!/bin/bash

# Sicherstellen, dass das Skript mit sudo ausgeführt wird
if [[ "$(id -u)" != "0" ]]; then
    echo "Dieses Skript muss mit sudo-Rechten ausgeführt werden." >&2
    exit 1
fi

LOGFILE="/var/log/django_oscar_install.log"
exec > >(tee -a "$LOGFILE") 2>&1

echo "Beginn der Installation..."

# Systemaktualisierungen
echo "Aktualisiere das System..."
apt-get update && apt-get upgrade -y

# Notwendige Pakete installieren
echo "Installiere notwendige Pakete..."
apt-get install -y python3-pip python3-dev python3-venv libpq-dev nginx git build-essential libssl-dev libffi-dev

# Installiere wheel global
pip3 install wheel

# Projektverzeichnis erstellen und Virtual Environment einrichten
echo "Erstelle Projektverzeichnis und richte Virtual Environment ein..."
mkdir -p /opt/pollwerkshop
cd /opt/pollwerkshop
python3 -m venv venv
source venv/bin/activate

# Pause zur Überprüfung des Virtual Environments
read -p "Virtual Environment ist aktiviert. Drücke Enter, um fortzufahren..."

# Wheel erneut im Virtual Environment installieren
pip install wheel

# Django und Django Oscar installieren
echo "Installiere Django und Django Oscar..."
pip install django oscar

# Django-Projekt erstellen
echo "Erstelle das Django-Projekt 'pollwerkshop'..."
django-admin startproject pollwerkshop .

# Oscar in das Django-Projekt integrieren
echo "Integriere Oscar in das Django-Projekt..."
cd pollwerkshop
# Füge hier die Integrationsschritte für Django Oscar ein

# Zurück zum Projekt-Root
cd ..

# Migrationen durchführen und statische Dateien sammeln
echo "Führe Migrationen durch und sammle statische Dateien..."
./manage.py migrate
./manage.py collectstatic --noinput --clear

# Nginx konfigurieren
echo "Konfiguriere Nginx..."
rm /etc/nginx/sites-enabled/default
cat > /etc/nginx/sites-available/pollwerkshop <<EOF
server {
    listen 80;
    server_name 123.456.78.90;  # Ersetze dies durch deine tatsächliche IP-Adresse oder Domain

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        alias /opt/pollwerkshop/static/;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/opt/pollwerkshop/pollwerkshop.sock;
    }
}
EOF

# Aktiviere die Nginx-Konfiguration und starte Nginx neu
ln -s /etc/nginx/sites-available/pollwerkshop /etc/nginx/sites-enabled
nginx -t && systemctl restart nginx

echo "Installation abgeschlossen. Überprüfe das Logfile für Details: $LOGFILE"
